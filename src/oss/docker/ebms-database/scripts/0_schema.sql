
create schema if not exists ebms;

create table ebms.cpa
(
    cpa_id varchar(256) not null
        primary key,
    cpa    text         not null
);

alter table ebms.cpa
    owner to ebms;

create table ebms.ebms_message
(
    time_stamp        timestamp    not null,
    cpa_id            varchar(256) not null
        references ebms.cpa,
    conversation_id   varchar(256) not null,
    message_id        varchar(256) not null
        primary key,
    ref_to_message_id varchar(256),
    time_to_live      timestamp,
    from_party_id     varchar(256) not null,
    from_role         varchar(256),
    to_party_id       varchar(256) not null,
    to_role           varchar(256),
    service           varchar(256) not null,
    action            varchar(256) not null,
    content           text,
    status            smallint,
    status_time       timestamp,
    persist_time      timestamp
);

alter table ebms.ebms_message
    owner to ebms;

create index i_ebms_ref_to_message
    on ebms.ebms_message (ref_to_message_id);

create table ebms.ebms_attachment
(
    message_id   varchar(256) not null
        references ebms.ebms_message,
    order_nr     smallint     not null,
    name         varchar(256),
    content_id   varchar(256) not null,
    content_type varchar(255) not null,
    content      bytea        not null,
    constraint uc_ebms_attachment
        unique (message_id, order_nr)
);

alter table ebms.ebms_attachment
    owner to ebms;

create table ebms.delivery_task
(
    cpa_id             varchar(256)           not null
        constraint ebms_event_cpa_id_fkey
            references ebms.cpa,
    receive_channel_id varchar(256)           not null,
    message_id         varchar(256)           not null
        constraint ebms_event_message_id_key
            unique
        references ebms.ebms_message,
    time_to_live       timestamp,
    time_stamp         timestamp              not null,
    retries            smallint default 0     not null,
    is_confidential    boolean  default false not null,
    server_id          varchar(256),
    send_channel_id    varchar(256)
);

alter table ebms.delivery_task
    owner to ebms;

create index i_delivery_task
    on ebms.delivery_task (time_stamp);

create table ebms.delivery_log
(
    message_id    varchar(256) not null
        references ebms.ebms_message,
    time_stamp    timestamp    not null,
    uri           varchar(256),
    status        smallint     not null,
    error_message text
);

alter table ebms.delivery_log
    owner to ebms;

create table ebms.url_mapping
(
    source      varchar(256) not null
        constraint url_source_key
            unique,
    destination varchar(256) not null
);

alter table ebms.url_mapping
    owner to ebms;

create table ebms.message_event
(
    message_id varchar(256)       not null
        constraint ebms_message_event_message_id_key
            unique
        references ebms.ebms_message,
    event_type smallint           not null,
    time_stamp timestamp          not null,
    processed  smallint default 0 not null
);

alter table ebms.message_event
    owner to ebms;

create index i_message_event
    on ebms.message_event (time_stamp);

create table ebms.certificate_mapping
(
    id          varchar(256) not null,
    source      bytea        not null,
    destination bytea        not null,
    cpa_id      varchar(256),
    unique (id, cpa_id)
);

alter table ebms.certificate_mapping
    owner to ebms;

create table ebms.qrtz_job_details
(
    sched_name        varchar(120) not null,
    job_name          varchar(200) not null,
    job_group         varchar(200) not null,
    description       varchar(250),
    job_class_name    varchar(250) not null,
    is_durable        boolean      not null,
    is_nonconcurrent  boolean      not null,
    is_update_data    boolean      not null,
    requests_recovery boolean      not null,
    job_data          bytea,
    primary key (sched_name, job_name, job_group)
);

alter table ebms.qrtz_job_details
    owner to ebms;

create index idx_qrtz_j_req_recovery
    on ebms.qrtz_job_details (sched_name, requests_recovery);

create index idx_qrtz_j_grp
    on ebms.qrtz_job_details (sched_name, job_group);

create table ebms.qrtz_triggers
(
    sched_name     varchar(120) not null,
    trigger_name   varchar(200) not null,
    trigger_group  varchar(200) not null,
    job_name       varchar(200) not null,
    job_group      varchar(200) not null,
    description    varchar(250),
    next_fire_time bigint,
    prev_fire_time bigint,
    priority       integer,
    trigger_state  varchar(16)  not null,
    trigger_type   varchar(8)   not null,
    start_time     bigint       not null,
    end_time       bigint,
    calendar_name  varchar(200),
    misfire_instr  smallint,
    job_data       bytea,
    primary key (sched_name, trigger_name, trigger_group),
    foreign key (sched_name, job_name, job_group) references ebms.qrtz_job_details
);

alter table ebms.qrtz_triggers
    owner to ebms;

create index idx_qrtz_t_j
    on ebms.qrtz_triggers (sched_name, job_name, job_group);

create index idx_qrtz_t_jg
    on ebms.qrtz_triggers (sched_name, job_group);

create index idx_qrtz_t_c
    on ebms.qrtz_triggers (sched_name, calendar_name);

create index idx_qrtz_t_g
    on ebms.qrtz_triggers (sched_name, trigger_group);

create index idx_qrtz_t_state
    on ebms.qrtz_triggers (sched_name, trigger_state);

create index idx_qrtz_t_n_state
    on ebms.qrtz_triggers (sched_name, trigger_name, trigger_group, trigger_state);

create index idx_qrtz_t_n_g_state
    on ebms.qrtz_triggers (sched_name, trigger_group, trigger_state);

create index idx_qrtz_t_next_fire_time
    on ebms.qrtz_triggers (sched_name, next_fire_time);

create index idx_qrtz_t_nft_st
    on ebms.qrtz_triggers (sched_name, trigger_state, next_fire_time);

create index idx_qrtz_t_nft_misfire
    on ebms.qrtz_triggers (sched_name, misfire_instr, next_fire_time);

create index idx_qrtz_t_nft_st_misfire
    on ebms.qrtz_triggers (sched_name, misfire_instr, next_fire_time, trigger_state);

create index idx_qrtz_t_nft_st_misfire_grp
    on ebms.qrtz_triggers (sched_name, misfire_instr, next_fire_time, trigger_group, trigger_state);

create table ebms.qrtz_simple_triggers
(
    sched_name      varchar(120) not null,
    trigger_name    varchar(200) not null,
    trigger_group   varchar(200) not null,
    repeat_count    bigint       not null,
    repeat_interval bigint       not null,
    times_triggered bigint       not null,
    primary key (sched_name, trigger_name, trigger_group),
    foreign key (sched_name, trigger_name, trigger_group) references ebms.qrtz_triggers
);

alter table ebms.qrtz_simple_triggers
    owner to ebms;

create table ebms.qrtz_cron_triggers
(
    sched_name      varchar(120) not null,
    trigger_name    varchar(200) not null,
    trigger_group   varchar(200) not null,
    cron_expression varchar(120) not null,
    time_zone_id    varchar(80),
    primary key (sched_name, trigger_name, trigger_group),
    foreign key (sched_name, trigger_name, trigger_group) references ebms.qrtz_triggers
);

alter table ebms.qrtz_cron_triggers
    owner to ebms;

create table ebms.qrtz_simprop_triggers
(
    sched_name    varchar(120) not null,
    trigger_name  varchar(200) not null,
    trigger_group varchar(200) not null,
    str_prop_1    varchar(512),
    str_prop_2    varchar(512),
    str_prop_3    varchar(512),
    int_prop_1    integer,
    int_prop_2    integer,
    long_prop_1   bigint,
    long_prop_2   bigint,
    dec_prop_1    numeric(13, 4),
    dec_prop_2    numeric(13, 4),
    bool_prop_1   boolean,
    bool_prop_2   boolean,
    primary key (sched_name, trigger_name, trigger_group),
    constraint qrtz_simprop_triggers_sched_name_trigger_name_trigger_grou_fkey
        foreign key (sched_name, trigger_name, trigger_group) references ebms.qrtz_triggers
);

alter table ebms.qrtz_simprop_triggers
    owner to ebms;

create table ebms.qrtz_blob_triggers
(
    sched_name    varchar(120) not null,
    trigger_name  varchar(200) not null,
    trigger_group varchar(200) not null,
    blob_data     bytea,
    primary key (sched_name, trigger_name, trigger_group),
    foreign key (sched_name, trigger_name, trigger_group) references ebms.qrtz_triggers
);

alter table ebms.qrtz_blob_triggers
    owner to ebms;

create table ebms.qrtz_calendars
(
    sched_name    varchar(120) not null,
    calendar_name varchar(200) not null,
    calendar      bytea        not null,
    primary key (sched_name, calendar_name)
);

alter table ebms.qrtz_calendars
    owner to ebms;

create table ebms.qrtz_paused_trigger_grps
(
    sched_name    varchar(120) not null,
    trigger_group varchar(200) not null,
    primary key (sched_name, trigger_group)
);

alter table ebms.qrtz_paused_trigger_grps
    owner to ebms;

create table ebms.qrtz_fired_triggers
(
    sched_name        varchar(120) not null,
    entry_id          varchar(95)  not null,
    trigger_name      varchar(200) not null,
    trigger_group     varchar(200) not null,
    instance_name     varchar(200) not null,
    fired_time        bigint       not null,
    sched_time        bigint       not null,
    priority          integer      not null,
    state             varchar(16)  not null,
    job_name          varchar(200),
    job_group         varchar(200),
    is_nonconcurrent  boolean,
    requests_recovery boolean,
    primary key (sched_name, entry_id)
);

alter table ebms.qrtz_fired_triggers
    owner to ebms;

create index idx_qrtz_ft_trig_inst_name
    on ebms.qrtz_fired_triggers (sched_name, instance_name);

create index idx_qrtz_ft_inst_job_req_rcvry
    on ebms.qrtz_fired_triggers (sched_name, instance_name, requests_recovery);

create index idx_qrtz_ft_j_g
    on ebms.qrtz_fired_triggers (sched_name, job_name, job_group);

create index idx_qrtz_ft_jg
    on ebms.qrtz_fired_triggers (sched_name, job_group);

create index idx_qrtz_ft_t_g
    on ebms.qrtz_fired_triggers (sched_name, trigger_name, trigger_group);

create index idx_qrtz_ft_tg
    on ebms.qrtz_fired_triggers (sched_name, trigger_group);

create table ebms.qrtz_scheduler_state
(
    sched_name        varchar(120) not null,
    instance_name     varchar(200) not null,
    last_checkin_time bigint       not null,
    checkin_interval  bigint       not null,
    primary key (sched_name, instance_name)
);

alter table ebms.qrtz_scheduler_state
    owner to ebms;

create table ebms.qrtz_locks
(
    sched_name varchar(120) not null,
    lock_name  varchar(40)  not null,
    primary key (sched_name, lock_name)
);

alter table ebms.qrtz_locks
    owner to ebms;

