This Docker Compose file can be used for the baseline Opensource implementation of OSDBK LPC.
Example usecase: Building a Proof of Concept from OSDBK LPC for evaluation purposes

Run opensource stack of ebms-core by executing the following from project root:
```docker-compose -f docker-compose_oss.yml --env-file src/oss/docker/.env up --build```

# To note:
This provides a baseline implementation only to illustrate the use of OSDBK LPC software stack.
This is intended to support a number of basic ebMS2 adapter use-cases namely:
- CPA Upload & sync (from cpa-service to ebms-core)
- ebMS2 Ping/Pong - ASYNC only (see 1. and 2. below)
- ebMS2 Single Attachment (see 1. and 2. below)
1. Please note that you need to provide your own ebMS infrastructure to test against
2. Examples such as ebms-stubs to simulate the processing or receiving of ebMS2

## Guidelines and FAQ:
- Supports amd64/x64 compatibility only; ARM platforms such as aarch64 Apple M1 etc silicon are not well-supported
- Recommended minimum 16GB system RAM
- Tested for use in an IPv4 deployment environment
