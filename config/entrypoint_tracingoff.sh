#!/bin/sh
java -Dlog4j.configurationFile=log4j2.xml $ATOMIKOS_ARGS -cp app.jar:lib/* nl.clockwork.ebms.admin.StartEmbedded -port 8888 -health -authentication "$@"
