#!/bin/sh
java -javaagent:opentelemetry-javaagent.jar -agentlib:jdwp=transport=dt_socket,address=*:5005,server=y,suspend=n -Dlog4j.configurationFile=log4j2.xml $ATOMIKOS_ARGS -cp app.jar:lib/* nl.clockwork.ebms.admin.StartEmbedded -port 8888 -health -authentication "$@"
