# EbMS Core / EbMS Admin & OSDBK

EbMS Core is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of seven applications:

- EbMS Core / EbMS Admin
- JMS Producer
- JMS Consumer
- CPA Service
- OSDBK Admin Console
- Apache ActiveMQ
- Throttling Service (optional)

EbMS Core / EbMS Admin is the backbone of OSDBK. It handles incoming and outgoing messages according to the EbMS 2.0 protocol (http://www.ebxml.org/specs/ebMS2.pdf).

The EbMS Adapter (including EbMS Core and EbMS Admin) is third party software, developed by Edwin Luinstra. Extensive documentation and configuration options of EbMS Admin can be found here: https://eluinstra.github.io/ebms-admin/. The GitHub-repository can be found here: https://github.com/eluinstra/ebms-core

For each incoming EbMS Message, EbMS Core sends an event to the RECEIVED queue on ActiveMQ, that is then picked up by the JMS Producer and routed to the correct queue.

Outbound messages are received from the JMS Consumer, that reads them a number of queues of ActiveMQ. The JMS Consumer retrieves the matching CPA for the message from the CPA Service and calls the REST Message Service of EbMS Core to provide the message and its routing information. EbMS Core will create an event for the outgoing message, which will be sent as soon as a thread becomes available.

The OSDBK Admin Console can be used to query and manage the CPA database. Additionally, it also provides functionality to resend failed and expired events. 

Optionally, the Throttling Service can be deployed that queries the EbMS database and returns a boolean to signal if an afnemer should be throttled. 


## Configuration
A list of configurable properties of EbMS Core / EbMS Admin can be found here: https://eluinstra.github.io/ebms-admin/ebms-admin/properties.html

#### DockerFile
In the DockerFile environment variables can be added to override the default properties.

By default, certain properties are set in the DockerFile to make EbMS Core function as it is in the OSDBK context. These values should not be altered if you intend to use EbMS Core together with the rest of the OSDBK stack.
~~~
# default property values
ENV DELIVERYMANAGER_TYPE JMS
ENV DELIVERYTASKHANDLER_TYPE JMS
ENV EVENTLISTENER_TYPE JMS
ENV EVENTLISTENER_FILTER DELIVERED
ENV TRANSACTIONMANAGER_TYPE ATOMIKOS
ENV TRANSACTIONMANAGER_ISOLATIONLEVEL TRANSACTION_READ_COMMITTED
ENV EBMS_JDBC_DRIVERCLASSNAME org.postgresql.xa.PGXADataSource
ENV LOGGING_MDC ENABLED
ENV CACHE_TYPE IGNITE
~~~

Other properties should be overridden, for example:
~~~
# values that should be overwritten on run
ENV EBMS_JDBC_URL jdbc:postgresql://ebms-database:5432/ebms
ENV JMS_BROKERURL tcp://activemq:61616
~~~

#### TLS on/offloading
EbMS Core can be configured to do TLS on/offloading. See https://eluinstra.github.io/ebms-admin/ebms-admin/command.html for more details.

By default, SSL had been disabled in the DockerFile:
~~~
# Environment override for port and SSL won't stick, so output to properties file.
RUN printf ebms.ssl=false\\nebms.port=8080 > ebms-admin.embedded.properties
~~~

#### Basic Authentication
The EbMS Core webservice has been secured using basic authentication. See https://eluinstra.github.io/ebms-admin/ebms-admin/command.html for more details.
A realm.properties file has been added in the DockerFile that contains the basic authentication credentials.

The credentials can be overwritten by defining a new HashUserRealm in realm.properties:
~~~
<username>: <password>[,<rolename> ...]
~~~

#### XA-transactions
EbMS Core and ActiveMQ exchange messages using XA-transactions (two-phase commit protocol). EbMS Core uses Atomikos as Transaction Manager. Both applications use XA-transactions for their databases as well.

In the DockerFile the default properties are overridden by the following environment variables:
~~~
ENV TRANSACTIONMANAGER_TYPE ATOMIKOS
ENV TRANSACTIONMANAGER_ISOLATIONLEVEL TRANSACTION_READ_COMMITTED
ENV EBMS_JDBC_DRIVERCLASSNAME org.postgresql.xa.PGXADataSource
~~~
To enable XA-transactions, the configuration of Postgres database needs an adjustment. The setting max_prepared_transactions needs to be greater than 0 (the default value). Notice that the settings max_prepared_transactions needs to match or be greater than the value of the setting max_transactions. In a default postgres installation this setting can be found in the postgresql.conf file.

#### Open Telemetry Tracing
By default, the DockerFile copies an  Open Telemetry Java jar onto the classpath of EbMS Core. When setting the environment variables, traces can be send to a tracing tool of choice, for example:
~~~
- OTEL_TRACES_EXPORTER=jaeger
- OTEL_EXPORTER_JAEGER_ENDPOINT=http://jaeger-tracing:14250
- OTEL_SERVICE_NAME=ebms-core
~~~
Tracing is enabled or disabled by choosing one of the entrypoint scripts, entrypoint.sh or entrypoint_tracingoff.sh respectively.

## CPA's
A Collaboration Protocol Agreement (CPA) is an essential part of the EbMS Protocol. Two parties exchanging messages via EbMS need to have a CPA loaded in their EbMS Adapter that confirms their agreement.

CPA's can be loaded into the database of EbMS Core / EbMS Admin by making use of the OSDBK Admin Console.

The CPA Service as of version 1.6.0 still needs to synchronize the CPA database with the Ebms Core CPA database, so that the latter is a mirror of the former. In future versions, Ebms Core will use the CPA database as its source.

At startup, EbMS Core loads all CPA's from the database into its cache. When adding, removing or updating a CPA in the OSDBK Admin Console, the cpa cache of the EbMS Core gets flushed so that the changes can be reloaded into the cache.

## Examples

#### Example ActiveMQ Configuration (activemq.xml)
To get up and running quickly with the OSDBK-stack, an example configuration of ActiveMQ can be found in the folder /docker/activemq/activemq.xml. It contains pre-configured queues and authorizations.

#### Example EbMS Message
~~~
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eb="http://www.oasis-open.org/committees/ebxml-msg/schema/msg-header-2_0.xsd" xmlns:ns4="http://www.w3.org/2000/09/xmldsig#" xmlns:xlink="http://www.w3.org/1999/xlink">
    <soap:Header>
        <eb:MessageHeader eb:version="2.0" soap:mustUnderstand="1">
            <eb:From>
                <eb:PartyId eb:type="urn:osb:oin">12345678901234567890</eb:PartyId>
                <eb:Role>LANDELIJKEVOORZIENING</eb:Role>
            </eb:From>
            <eb:To>
                <eb:PartyId eb:type="urn:osb:oin">09876543210987654321</eb:PartyId>
                <eb:Role>DIGILEVERING</eb:Role>
            </eb:To>
            <eb:CPAId>${cpaid1}</eb:CPAId>
            <eb:ConversationId>aaaa-bbbb-cccc</eb:ConversationId>
            <eb:Service eb:type="urn:osb:services">${dienst}</eb:Service>
            <eb:Action>verstrekkingDoorLV</eb:Action>
            <eb:MessageData>
                <eb:MessageId>end2end_${messageID}_${conversationID}</eb:MessageId>
                <eb:Timestamp>${timestamp}</eb:Timestamp>
                <eb:TimeToLive>${timetolive}</eb:TimeToLive>
            </eb:MessageData>
            <eb:DuplicateElimination/>
        </eb:MessageHeader>
        <eb:AckRequested eb:signed="false" eb:version="2.0" soap:actor="urn:oasis:names:tc:ebxml-msg:actor:toPartyMSH" soap:mustUnderstand="1"/>
    </soap:Header>
    <soap:Body>
        <eb:Manifest eb:version="2.0">
            <eb:Reference xlink:href="cid:${attachmentID}" xlink:type="simple"/>
        </eb:Manifest>
    </soap:Body>
</soap:Envelope>
~~~

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However the following variables need to be populated by means of a `.env` file:
~~~
${OSDBK_IMAGE_REPOSITORY} -- location of your Docker repository where you are housing your built OSDBK docker images
${ACTIVEMQ_IMAGE_TAG} -- the tag of your built ActiveMQ docker image
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
${DGL_STUB_IMAGE_TAG}  -- the tag of your built stub application docker image, to simulate interactions with an external party
${DGL_STUB_MESSAGE_SENDER_OIN_AFNEMER} -- the OIN of your stubbed application
${EBMS_JDBC_USERNAME} -- the username used to connect to the EBMS database
${EBMS_JDBC_PASSWORD} -- the password used to connect to the EBMS database
${EBMS_JMS_BROKER_USERNAME} -- the username used to connect the ebms application to the ActiveMQ instance
${EBMS_JMS_BROKER_PASSWORD} -- the password used to connect the ebms application to the ActiveMQ instance
${EBMS_POSTGRESQL_IMAGE_TAG} -- the tag of your built EBMS PostgresQL docker image
${EBMS_STUB_IMAGE_TAG} -- the tag of your built stub application docker image, to simulate interactions with an external party
${JMS_CONSUMER_IMAGE_TAG} -- the tag of your built jms-consumer docker image
${JMS_CONSUMER_ACTIVEMQ_USER} -- the username used to connect the jms-consumer application to the ActiveMQ instance
${JMS_CONSUMER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-consumer application to the ActiveMQ instance
${JMS_PRODUCER_IMAGE_TAG} -- the tag of your built jms-producer docker image
${JMS_PRODUCER_ACTIVEMQ_USER} -- the username used to connect the jms-producer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-producer application to the ActiveMQ instance
${THROTTLING_SERVICE_IMAGE_TAG} -- the tag of your built throttling docker image
${CPA_SERVICE_IMAGE_TAG} -- the tag of your built cpa-service docker image
${OSDBK_ADMIN_CONSOLE_IMAGE_TAG} -- the tag of your built OSDBK Admin console docker image
${CPA_JDBC_USERNAME} -- the username used to connect to the CPA database
${CPA_JDBC_PASSWORD} -- the password used to connect to the CPA database
${CPA_SERVICE_ACTIVEMQ_USER} -- the username used to connect the cpa-service application to the ActiveMQ instance
${CPA_SERVICE_ACTIVEMQ_PASSWORD} -- the password used to connect the cpa-service application to the ActiveMQ instance
${CPA_DATABASE_IMAGE_TAG} -- the tag of your built CPA PostgresQL docker image
~~~

You can run the OSDBK stack on your machine by referencing the .env file which will populate the above ENV values needed by docker-compose
#### Example of how to execute docker-compose using existing environment file
```
docker-compose --env-file ../../../../git.lpc.logius.nl/open/pgu/osdbk/lpc/deployments/osdbk-chart/docker-compose-environment/.env up --build
```

## Release notes
See [NOTES][NOTES] for latest.

[NOTES]: templates/NOTES.txt

### Older release notes collated below:

V 2.19.1-1
- NOTES added as part of code to enrich installation process output
- 2.19.1 core reused
V 2.19.1 - 2.19.1-1-SNAPSHOT
- Various improvements to:
  - release notes as part of code
  - container technology
  - use of base-images
  - GITLAB Security scan framework implemented
  - Improved Open Source build process and container releases
  - Test improvements via docker-compose
  - Dependency upgrades
  - Mavenization of lib build
