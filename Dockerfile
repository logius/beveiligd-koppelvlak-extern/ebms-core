FROM registry.gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin:11-jre-alpine-opentelemetry

#See https://gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin/-/blob/11-jre-alpine-opentelemetry/Dockerfile?ref_type=heads
ARG ALPINE_VERSION=v3.17

ARG EBMS_VERSION=2.19.2
ARG EBMS_ADMIN=ebms-admin-${EBMS_VERSION}.jar

# user should be numeric to comply to Kubernetes PodSecurityPolicy runAsNonRoot
ARG EBMS_UID=1001

ARG POSTGRES_DRIVER_VERSION=42.6.1
ARG POSTGRES_DRIVER=postgresql-${POSTGRES_DRIVER_VERSION}.jar

# DEPENDENCY FOR IGNITE AUTODISCOVERY ON KUBERNETES
ARG IGNITE_KUBERNETES_DEPENDENCY_VERSION=2.16.0
ARG IGNITE_KUBERNETES_DEPENDENCY=ignite-kubernetes-${IGNITE_KUBERNETES_DEPENDENCY_VERSION}.jar

ARG LOG4J_JSON_DRIVER_VERSION=2.17.2
ARG LOG4J_JSON_DRIVER_DEPENDENCY=log4j-layout-template-json-${LOG4J_JSON_DRIVER_VERSION}.jar
ARG M2_REPO_URL=https://repo1.maven.org/maven2

WORKDIR /opt/app

USER root

RUN apk add \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      curl zip
RUN getIt() { curl -kLS --retry-all-errors --retry 10 --retry-delay 1 ${1} -o ${2}; } && \
  getIt https://github.com/eluinstra/ebms-admin/releases/download/ebms-admin-${EBMS_VERSION}/${EBMS_ADMIN} app.jar && \
  mkdir -p lib && \
  getIt https://jdbc.postgresql.org/download/${POSTGRES_DRIVER} lib/${POSTGRES_DRIVER} && \
# ADD MISSING DEPENDENCY FOR IGNITE AUTODISCOVERY ON KUBERNETES
  getIt ${M2_REPO_URL}/org/apache/ignite/ignite-kubernetes/${IGNITE_KUBERNETES_DEPENDENCY_VERSION}/${IGNITE_KUBERNETES_DEPENDENCY} lib/${IGNITE_KUBERNETES_DEPENDENCY} && \
# ADD MISSING log4j-layout-template-json
  getIt ${M2_REPO_URL}/org/apache/logging/log4j/log4j-layout-template-json/${LOG4J_JSON_DRIVER_VERSION}/${LOG4J_JSON_DRIVER_DEPENDENCY} lib/${LOG4J_JSON_DRIVER_DEPENDENCY} && \
  zip -d lib/${LOG4J_JSON_DRIVER_DEPENDENCY} META-INF/org/apache/logging/log4j/core/config/plugins/Log4j2Plugins.dat

# COPY realm.properties to configure basic authentication on the ebms endpoints
COPY config/realm.properties realm.properties

# Copy ignite config jar to /lib and cleanup the ignite.xml
COPY target/${IGNITE_CONFIG_JAR} lib/

# Create user to comply to numeric UID for k8s. This workaround should be adopted in the base image.
RUN addgroup ebms && adduser -S -h /home/ebms -G ebms -u ${EBMS_UID} ebms && \
  chown -R ebms:ebms /opt/app && \
  chown -h ebms:ebms /home/ebms

COPY config/entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh

COPY config/entrypoint_tracingoff.sh entrypoint_tracingoff.sh
RUN chmod +x entrypoint_tracingoff.sh

COPY config/entrypoint_debug.sh entrypoint_debug.sh
RUN chmod +x entrypoint_debug.sh

RUN apk del \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      curl zip

USER $EBMS_UID

COPY config/log4j2.xml log4j2.xml
COPY config/log4j2-json-layout.json log4j2-json-layout.json

# Environment override for port and SSL won't stick, so output to properties file.
RUN printf ebms.ssl=false\\nebms.port=8080 > ebms-admin.embedded.properties

# default property values
ENV DELIVERYMANAGER_TYPE JMS
ENV DELIVERYTASKHANDLER_TYPE JMS
ENV EVENTLISTENER_TYPE JMS
ENV EVENTLISTENER_FILTER DELIVERED
ENV TRANSACTIONMANAGER_TYPE ATOMIKOS
ENV TRANSACTIONMANAGER_ISOLATIONLEVEL TRANSACTION_READ_COMMITTED
ENV EBMS_JDBC_DRIVERCLASSNAME org.postgresql.xa.PGXADataSource
ENV LOGGING_MDC ENABLED
ENV CACHE_TYPE IGNITE
ENV SERVER_JETTY_MAX-HTTP-FORM-POST-SIZE 20MB

# values that should be overwritten on run
ENV EBMS_JDBC_URL jdbc:postgresql://ebms-database:5432/ebms
ENV JMS_BROKERURL tcp://activemq:61616

ENV ATOMIKOS_ARGS "-Dcom.atomikos.icatch.max_actives=-1 -Dcom.atomikos.icatch.output_dir=/opt/app/log -Dcom.atomikos.icatch.log_base_dir=/opt/app/log"

EXPOSE 8080 8888 8008 47500 47100

ENTRYPOINT ["./entrypoint.sh"]

CMD ["-headless", "-soap"]
